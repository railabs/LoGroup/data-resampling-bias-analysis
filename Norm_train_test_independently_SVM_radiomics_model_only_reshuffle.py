## python -W ignore ..py
## Created Date: August 28, 2023
## Name: Rui Hou
## Study: Data Resampling Bias
## Requirement: Python 2.7
## python -W ignore ..py
import sys,os,random,math,time,glob,csv,scipy.io,copy,xlrd,datetime,xlwt,dicom,pydicom,cv2
import numpy as np
from scipy import interp
# import matplotlib.pyplot as plt
from itertools import cycle
from sklearn import svm, metrics, linear_model,preprocessing
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import RandomizedLogisticRegression
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from skimage.morphology import convex_hull_image
from skimage.measure import regionprops,label
from skimage.feature.texture import greycomatrix,greycoprops
from scipy.spatial.distance import pdist,cdist
from scipy.misc import imread,imsave

import warnings
with warnings.catch_warnings():
	warnings.filterwarnings("ignore", category=DeprecationWarning) 
	warnings.filterwarnings("ignore", category=RuntimeWarning)
	warnings.filterwarnings("ignore", category=FutureWarning)
	# warnings.filterwarnings("ignore", category=ConvergenceWarning)

def mean_std_normalize(X_new_tr,X_new_val):
	mean_std_scaler0 = preprocessing.StandardScaler(copy=True, with_mean=True, with_std=True)
	X_tr = mean_std_scaler0.fit_transform(X_new_tr)
	if len(X_new_val)>0:
		X_val = mean_std_scaler0.transform(X_new_val)
	else:
		X_val = []
	return X_tr,X_val


if __name__ == '__main__':
	num_reshuffle_times = int(sys.argv[1]) #60 
	iterTimes = int(sys.argv[2]) #200 
	num_cpus_to_load = 20
	cluster_features_names = ['MCC Area', 'MCC Eccentricity', 'MCC NO of MCs',  'MCC MCs Coverage', 'MCC Background Mean', 'MCC Background Std', 'MCC All MCs Intensity Mean', 'MCC All MCs Intensity Std', 'Mean GLCM Contrast', 'Mean GLCM Correlation', 'Mean GLCM Energy', 'Mean GLCM Homogeneity', 'Lesion Size']
	ind_features_names = ['MC Perimeter', 'MC Area', 'MC Circularity', 'MC Eccentricity', 'MC Major Axis', 'MC Minor Axis', '1st Variant of Hu Moments','2nd Variant of Hu Moments','3rd Variant of Hu Moments','4th Variant of Hu Moments','5th Variant of Hu Moments','6th Variant of Hu Moments','7th Variant of Hu Moments','MC Distance to Cluster Centroid','MC Distance to Nearest MC','MC Normalized Degree','MC Intensity Mean','MC Intensity Std','MC Background Intensity Mean','MC Background Intensity Std','MC Mean GLCM Contrast','MC Mean GLCM Correlation','MC Mean GLCM Energy','MC Mean GLCM Homogeneity']
	radiomics_feat_list =	cluster_features_names[:-1] + [' '.join(['Mean',ss]) for ss in ind_features_names] + [' '.join(['Std',ss]) for ss in ind_features_names] + \
						[' '.join(['Min',ss]) for ss in ind_features_names] + [' '.join(['Max',ss]) for ss in ind_features_names] + \
						cluster_features_names[-1:]

	clinical_feat_list = ['Nuclear Grade','ER','PR','Age']
	xlsx_file_name_alldcis = 'original_file.xlsx'
	####
	num_radiomic_features = len(radiomics_feat_list)
	num_clinical_features = 4
	########################################################################################
	all_train_test_results_per_shuffle = 'train_test_reshuffle_SVM-one_model_with_hyperparameter_tuning_all_aucs_rest.csv'
	renamed = 1
	while os.path.isfile(all_train_test_results_per_shuffle):
		all_train_test_results_per_shuffle = all_train_test_results_per_shuffle.replace('.csv','%d.csv'%(renamed))
		renamed += 1	
	with open(all_train_test_results_per_shuffle, 'w') as f_train_test_results_reshuffled:
		rea_train_test_results_reshuffled = csv.writer(f_train_test_results_reshuffled)
		rea_train_test_results_reshuffled.writerow(['Random Seed','P-value Age', 'P-value Lesion', 'Training Clinical Features Only AUC', 'Training Radiomics Features Only AUC', 'Training Radiomics Features With Feature Selection AUC', 'Training Radiomics Top Features AUC', 'Training Radiomics + Clinical Features AUC', 'Training Radiomics + Clinical Features With Feature Selection AUC', 'Testing Clinical Features Only AUC', 'Testing Radiomics Features Only AUC', 'Testing Radiomics Top Feats AUC', 'Testing Radiomics + Clinical Features AUC'])
	##
	random_list = random.sample(range(1,1000), num_reshuffle_times*2)	
	# random_list_subset = np.unique(random_list)[:num_reshuffle_times]
	# random_list_subset = [0, 8, 14, 16, 28, 35, 38, 318, 329, 337, 348, 364, 373] ## Re running on don
	random_list_subset = [374, 46, 50, 383, 398, 411, 419, 421, 437, 441, 824, 652, 588, 312, 453, 454, 467, 909, 210, 208, 468, 472, 475, 479, 489, 498, 511, 577, 624, 935, 76, 334, 34, 43, 445, 449, 492]
	for reshuffle_ids_now,random_seed_for_train_test_reshuffle in enumerate(random_list_subset):
		normalized_train_test_csv_saved_folder = 'no_hyperparater_SVM/random_shuffling_seed%d/' %(random_seed_for_train_test_reshuffle)
		###
		train_normalized_radiomics_clinical_file_name = normalized_train_test_csv_saved_folder + \
			'Train_400_features_CV_and_CLINICAL_' + xlsx_file_name_alldcis.split('/')[-1].replace('.xlsx','.csv')
		test_normalized_radiomics_clinical_file_name = normalized_train_test_csv_saved_folder + \
			'Test_300_features_CV_and_CLINICAL_' + xlsx_file_name_alldcis.split('/')[-1].replace('.xlsx','.csv')
		##***##***
		X_training_400_radiomics_and_clinical = np.array([]).reshape(0, num_radiomic_features + num_clinical_features)
		Y_training_400 = np.array([])
		with open(train_normalized_radiomics_clinical_file_name,'r') as f_feat_training_400_cli_and_cv:
			rea_training_400_cli_and_cv = csv.reader(f_feat_training_400_cli_and_cv)
			header_training_400_cli_and_cv = next(rea_training_400_cli_and_cv)
			for one_row in rea_training_400_cli_and_cv:
				one_dx = [int(one_row[1])]
				Y_training_400 = np.concatenate((Y_training_400,one_dx))
				X_one_feat = np.array(one_row[2:]).reshape(1, num_radiomic_features + num_clinical_features)
				X_training_400_radiomics_and_clinical = np.concatenate((X_training_400_radiomics_and_clinical, X_one_feat), axis = 0)

		X_training_400_radiomics = X_training_400_radiomics_and_clinical[:, :num_radiomic_features]
		X_training_400_clinical = X_training_400_radiomics_and_clinical[:, num_radiomic_features: ]
		######
		names_testing_300 = []
		X_testing_300_radiomics_and_clinical = np.array([]).reshape(0, num_radiomic_features + num_clinical_features)
		Y_testing_300 = np.array([])
		with open(test_normalized_radiomics_clinical_file_name,'r') as f_feat_testing_300_cli_and_cv:
			rea_testing_300_cli_and_cv = csv.reader(f_feat_testing_300_cli_and_cv)
			header_testing_300_cli_and_cv = next(rea_testing_300_cli_and_cv)
			for one_row in rea_testing_300_cli_and_cv:
				names_testing_300.append(str(one_row[0]))
				one_dx = [int(one_row[1])]
				Y_testing_300 = np.concatenate((Y_testing_300,one_dx))
				X_one_feat = np.array(one_row[2:]).reshape(1, num_radiomic_features + num_clinical_features)
				X_testing_300_radiomics_and_clinical = np.concatenate((X_testing_300_radiomics_and_clinical, X_one_feat), axis = 0)

		X_testing_300_radiomics = X_testing_300_radiomics_and_clinical[:, :num_radiomic_features]
		X_testing_300_clinical = X_testing_300_radiomics_and_clinical[:, num_radiomic_features: ]
		###########################################################################################
		nfold = 5
		embedded_cv = 4
		param_grid = [{'kernel': ['rbf'], 'gamma': [1e-1, 1e-2, 1e-3, 1e-4, 1e-5], 
				'C': [0.000001, 0.00001, 0.0001, 0.001, 0.10, 0.1, 10, 100, 1000]},
			{'kernel': ['sigmoid'], 'gamma': [1e-1, 1e-2, 1e-3, 1e-4, 1e-5],
				'C': [0.000001, 0.00001, 0.0001, 0.001, 0.10, 0.1, 10, 100, 1000]},
			{'kernel': ['linear'], 'gamma': ['scale'], 'C': [0.000001, 0.00001, 0.0001, 0.001, 0.10, 0.1, 100, 1000]}]
		#####
		saved_log_path  = 'training_results/results_norm_independently/SVM_with_hyperparameter_tuning_iter%d/random_shuffling_seed%d/' %(iterTimes, random_seed_for_train_test_reshuffle)
		if not os.path.isdir(saved_log_path):
			os.makedirs(saved_log_path)

		################################CV ONLY Logistic Regression with Feature Selection ######################
		f_log = open(saved_log_path + 'logs_CV_ONLY_LogisticRegression_WITH_and_WITHOUT_feature_selection_with_saving_predictions.log','a')
		## Model A on 400 Training Data with Logistic Regression
		num_training_pure = np.where(Y_training_400==0)[0].shape[0]
		num_training_upstaging = np.where(Y_training_400==1)[0].shape[0]
		num_holdout_pure = int(round(num_training_pure/float(nfold)))
		num_holdout_upstaging = int(round(num_training_upstaging/float(nfold)))
		##
		f_csv_cv_labels = open(saved_log_path + 'CV_ONLY_Labels_with_%dfolds_%diterations.csv'%(nfold,iterTimes),'w')
		wri_cv_labels = csv.writer(f_csv_cv_labels)
		##
		best_param_set_training_400_cv = []
		auc_training_400_cv_nofs = np.zeros([iterTimes,nfold])
		f_csv_cv_nofs = open(saved_log_path + 'CV_ONLY_NO-FS_Predictions_with_%dfolds_%diterations.csv'%(nfold,iterTimes),'w')
		wri_cv_nofs = csv.writer(f_csv_cv_nofs)
		##
		best_param_set_training_400_cv_with_fs = []
		auc_training_400_cv_with_fs = np.zeros([iterTimes,nfold])
		f_csv_cv_with_fs = open(saved_log_path + 'CV_ONLY_WITH-FS_Predictions_with_%dfolds_%diterations.csv'%(nfold,iterTimes),'w')
		wri_cv_with_fs = csv.writer(f_csv_cv_with_fs)
		##
		idx_feat_picked_training_400 = []
		feat_picked_training_400 = []
		ind0 = [indsub for indsub,x in enumerate(Y_training_400) if x==0]
		ind1 = [indsub for indsub,x in enumerate(Y_training_400) if x==1]
		for iter2,seed_val in zip(range(iterTimes),range(iterTimes)):	
			random.seed((iterTimes-seed_val)*2);random.shuffle(ind0)	
			random.seed((iterTimes-seed_val)*2);random.shuffle(ind1)
			for nf in range(nfold):
				ind_val = ind0[nf*num_holdout_pure:nf*num_holdout_pure+num_holdout_pure]+ind1[nf*num_holdout_upstaging:nf*num_holdout_upstaging+num_holdout_upstaging]
				ind_tr = list(set(ind0+ind1)-set(ind_val))
				X_new_tr = np.copy(X_training_400_radiomics[np.array(ind_tr),:])
				Y_tr = np.array(Y_training_400[np.array(ind_tr)])
				X_new_val = np.copy(X_training_400_radiomics[np.array(ind_val),:])
				Y_val = np.array(Y_training_400[np.array(ind_val)])
				wri_cv_labels.writerow(Y_val.tolist())
				X_tr = np.copy(X_new_tr);X_val = np.copy(X_new_val)
				lr_baseline_infold = GridSearchCV(svm.SVC(probability = True, max_iter = 20000), param_grid, cv=embedded_cv,scoring='roc_auc', n_jobs = num_cpus_to_load)
				_=lr_baseline_infold.fit(X_tr,Y_tr)
				param_set_training_400_cv_now = lr_baseline_infold.best_params_
				best_param_set_training_400_cv.append(param_set_training_400_cv_now)
				val_proba_now = lr_baseline_infold.predict_proba(X_val)[:,1]
				wri_cv_nofs.writerow(val_proba_now.tolist())
				auc_training_400_cv_nofs[iter2,nf] = metrics.roc_auc_score(Y_val,val_proba_now)
				##
			# 	randomized_logistic = RandomizedLogisticRegression(C=1,random_state=nfold)
			# 	randomized_logistic.fit(X_tr,Y_tr)
			# 	ind_fea = randomized_logistic.all_scores_[:,0]
			# 	rank_one_ind_fea = np.flipud(np.argsort(ind_fea))
			# 	current_idx_feat_picked = [i for i in rank_one_ind_fea if ind_fea[i]>0]
			# 	if len(current_idx_feat_picked) > 0:
			# 		current_feat_picked = np.array(radiomics_feat_list)[np.array(current_idx_feat_picked)]
			# 		idx_feat_picked_training_400.append(current_idx_feat_picked)
			# 		feat_picked_training_400.append(current_feat_picked)
			# 		X_tr = np.copy(X_new_tr[:,np.array(current_idx_feat_picked)])
			# 		X_val = np.copy(X_new_val[:,np.array(current_idx_feat_picked)])
			# 		lr_training_400_infold_with_fs = GridSearchCV(svm.SVC(probability = True, max_iter = 20000), param_grid, cv=embedded_cv,scoring='roc_auc', n_jobs = num_cpus_to_load)
			# 		_=lr_training_400_infold_with_fs.fit(X_tr,Y_tr)
			# 		param_set_training_400_cv_with_fs_now = lr_training_400_infold_with_fs.best_params_
			# 		best_param_set_training_400_cv_with_fs.append(param_set_training_400_cv_with_fs_now)
			# 		val_proba_now_with_fs = lr_training_400_infold_with_fs.predict_proba(X_val)[:,1]
			# 		wri_cv_with_fs.writerow(val_proba_now_with_fs.tolist())
			# 		auc_training_400_cv_with_fs[iter2,nf] = metrics.roc_auc_score(Y_val,val_proba_now_with_fs)
			# 	else:
			# 		auc_training_400_cv_with_fs[iter2,nf] = 0.5
			print('--------- CV Only SVM Run %d NO fs AUC: %0.3f +- %0.3f; WITH fs AUC: %0.3f +- %0.3f' \
				%(iter2+1, round(auc_training_400_cv_nofs[:iter2+1].mean(),3),round(auc_training_400_cv_nofs[:iter2+1].mean(1).std(),3), \
					round(auc_training_400_cv_with_fs[:iter2+1].mean(),3),round(auc_training_400_cv_with_fs[:iter2+1].mean(1).std(),3)) )
			f_log.write('--------- CV Only SVM Run %d NO fs AUC: %0.3f +- %0.3f; WITH fs AUC: %0.3f +- %0.3f\n' \
				%(iter2+1, round(auc_training_400_cv_nofs[:iter2+1].mean(),3),round(auc_training_400_cv_nofs[:iter2+1].mean(1).std(),3), \
					round(auc_training_400_cv_with_fs[:iter2+1].mean(),3),round(auc_training_400_cv_with_fs[:iter2+1].mean(1).std(),3)) )
		# np.save(saved_log_path+'CV_ONLY_Training_NO_FS_400_LogisticRegression_c.npy',best_c_training_400_cv)
		# np.save(saved_log_path+'CV_ONLY_Training_WITH_FS_400_LogisticRegression_c.npy',best_c_training_400_cv_with_fs)
		uni_param_set_cv,counts_param_set_cv = np.unique(best_param_set_training_400_cv,return_counts = True)
		most_selected_param_set_cv = uni_param_set_cv[np.flipud(np.argsort(counts_param_set_cv))][0]
		print('Most Selected Parameter Set for Radiomics ONLY:', most_selected_param_set_cv)
		print('--- Radiomics Only AUC: %0.3f +- %0.3f; WITH fs AUC: %0.3f +- %0.3f' %(round(auc_training_400_cv_nofs.mean(),3),round(auc_training_400_cv_nofs.mean(1).std(),3),round(auc_training_400_cv_with_fs.mean(),3),round(auc_training_400_cv_with_fs.mean(1).std(),3)))
		f_log.write('---- Radiomics Only AUC: %0.3f +- %0.3f; WITH fs AUC: %0.3f +- %0.3f\n' %(round(auc_training_400_cv_nofs.mean(),3),round(auc_training_400_cv_nofs.mean(1).std(),3),round(auc_training_400_cv_with_fs.mean(),3),round(auc_training_400_cv_with_fs.mean(1).std(),3)))
		f_log.close()

		np.save(saved_log_path+'CV_ONLY_Training_WITH_FS_400_LogisticRegression_picked_features_idx.npy',idx_feat_picked_training_400)
		f_feat_picked = open(saved_log_path + 'CV_ONLY_Training_WITH_FS_400_LogisticRegression_picked_features_names.txt','w')
		for one_select in feat_picked_training_400:
			f_feat_picked.write(' + '.join(one_select)+'\n')
		f_feat_picked.close()
		f_csv_cv_labels.close()
		f_csv_cv_nofs.close()
		f_csv_cv_with_fs.close()

		#########################CV AND CLINICAL FEATURES Logistic Regression with Feature Selection and Top Features Performance###############
		f_log = open(saved_log_path + 'logs_CLINICAL_plus_CV_LR_WITH_and_WITHOUT_feature_selection.log','a')
		## Model A on 400 Training Data with Logistic Regression
		num_training_pure = np.where(Y_training_400==0)[0].shape[0]
		num_training_upstaging = np.where(Y_training_400==1)[0].shape[0]
		num_holdout_pure = int(round(num_training_pure/float(nfold)))
		num_holdout_upstaging = int(round(num_training_upstaging/float(nfold)))
		##
		f_csv_cv_and_cli_labels = open(saved_log_path + 'CV_AND_Clinical_Labels_with_%dfolds_%diterations.csv'%(nfold,iterTimes),'w')
		wri_cv_and_cli_labels = csv.writer(f_csv_cv_and_cli_labels)
		##
		best_param_set_training_400_cli_and_cv = []
		auc_training_400_cli_and_cv_nofs = np.zeros([iterTimes,nfold])
		f_csv_cv_and_cli_nofs = open(saved_log_path + 'CV_AND_Clinical_NO-FS_Predictions_with_%dfolds_%diterations.csv'%(nfold,iterTimes),'w')
		wri_cv_and_cli_nofs = csv.writer(f_csv_cv_and_cli_nofs)
		##
		best_param_set_training_400_cli_and_cv_with_fs = []
		auc_training_400_cli_and_cv_with_fs = np.zeros([iterTimes,nfold])
		f_csv_cv_and_cli_with_fs = open(saved_log_path + 'CV_AND_Clinical_WITH-FS_Predictions_with_%dfolds_%diterations.csv'%(nfold,iterTimes),'w')
		wri_cv_and_cli_with_fs = csv.writer(f_csv_cv_and_cli_with_fs)
		##
		idx_feat_picked_training_400 = []
		feat_picked_training_400 = []
		ind0 = [indsub for indsub,x in enumerate(Y_training_400) if x==0]
		ind1 = [indsub for indsub,x in enumerate(Y_training_400) if x==1]
		for iter2,seed_val in zip(range(iterTimes),range(iterTimes)):	
			random.seed((iterTimes-seed_val)*2);random.shuffle(ind0)	
			random.seed((iterTimes-seed_val)*2);random.shuffle(ind1)
			for nf in range(nfold):
				ind_val = ind0[nf*num_holdout_pure:nf*num_holdout_pure+num_holdout_pure]+ind1[nf*num_holdout_upstaging:nf*num_holdout_upstaging+num_holdout_upstaging]
				ind_tr = list(set(ind0+ind1)-set(ind_val))
				X_new_tr = np.copy(X_training_400_radiomics_and_clinical[np.array(ind_tr),:])
				Y_tr = np.array(Y_training_400[np.array(ind_tr)])
				X_new_val = np.copy(X_training_400_radiomics_and_clinical[np.array(ind_val),:])
				Y_val = np.array(Y_training_400[np.array(ind_val)])
				wri_cv_and_cli_labels.writerow(Y_val.tolist())
				X_tr = np.copy(X_new_tr);X_val = np.copy(X_new_val)
				lr_baseline_infold = GridSearchCV(svm.SVC(probability = True, max_iter = 20000), param_grid, cv=embedded_cv,scoring='roc_auc', n_jobs = num_cpus_to_load)
				_=lr_baseline_infold.fit(X_tr,Y_tr)
				param_set_training_400_cli_and_cv_now = lr_baseline_infold.best_params_
				best_param_set_training_400_cli_and_cv.append(param_set_training_400_cli_and_cv_now)
				val_proba_now = lr_baseline_infold.predict_proba(X_val)[:,1]
				wri_cv_and_cli_nofs.writerow(val_proba_now.tolist())
				auc_training_400_cli_and_cv_nofs[iter2,nf] = metrics.roc_auc_score(Y_val,val_proba_now)
				##
				# randomized_logistic = RandomizedLogisticRegression(C=1,random_state=nfold)
				# randomized_logistic.fit(X_tr,Y_tr)
				# ind_fea = np.array([i[0] for i in randomized_logistic.all_scores_])
				# rank_one_ind_fea = np.flipud(np.argsort(ind_fea))
				# current_idx_feat_picked = [i for i in rank_one_ind_fea if ind_fea[i]>0]
				# if len(current_idx_feat_picked) > 0:
				# 	current_feat_picked = np.array(radiomics_feat_list + clinical_feat_list)[np.array(current_idx_feat_picked)]
				# 	idx_feat_picked_training_400.append(current_idx_feat_picked)
				# 	feat_picked_training_400.append(current_feat_picked)
				# 	X_tr = np.copy(X_new_tr[:,np.array(current_idx_feat_picked)])
				# 	X_val = np.copy(X_new_val[:,np.array(current_idx_feat_picked)])
				# 	lr_training_400_infold_with_fs = GridSearchCV(svm.SVC(probability = True, max_iter = 20000), param_grid, cv=embedded_cv,scoring='roc_auc', n_jobs = num_cpus_to_load)
				# 	_=lr_training_400_infold_with_fs.fit(X_tr,Y_tr)
				# 	param_set_training_400_cli_and_cv_now = lr_training_400_infold_with_fs.best_params_
				# 	best_param_set_training_400_cli_and_cv_with_fs.extend(param_set_training_400_cli_and_cv_now)
				# 	val_proba_now_with_fs = lr_training_400_infold_with_fs.predict_proba(X_val)[:,1]
				# 	wri_cv_and_cli_with_fs.writerow(val_proba_now_with_fs.tolist())
				# 	auc_training_400_cli_and_cv_with_fs[iter2,nf] = metrics.roc_auc_score(Y_val,val_proba_now_with_fs)
				# else:
				# 	auc_training_400_cli_and_cv_with_fs[iter2,nf] = 0.5
			print('--------- Clinical And CV SVM Run %d NO fs AUC: %0.3f +- %0.3f; WITH fs AUC: %0.3f +- %0.3f' \
				%(iter2+1, round(auc_training_400_cli_and_cv_nofs[:iter2+1].mean(),3), \
					round(auc_training_400_cli_and_cv_nofs[:iter2+1].mean(1).std(),3), \
					round(auc_training_400_cli_and_cv_with_fs[:iter2+1].mean(),3),round(auc_training_400_cli_and_cv_with_fs[:iter2+1].mean(1).std(),3)) )
			f_log.write('--------- Clinical And CV SVM Run %d NO fs AUC: %0.3f +- %0.3f; WITH fs AUC: %0.3f +- %0.3f\n' \
				%(iter2+1, round(auc_training_400_cli_and_cv_nofs[:iter2+1].mean(),3), \
					round(auc_training_400_cli_and_cv_nofs[:iter2+1].mean(1).std(),3), \
					round(auc_training_400_cli_and_cv_with_fs[:iter2+1].mean(),3),round(auc_training_400_cli_and_cv_with_fs[:iter2+1].mean(1).std(),3)) )
		# np.save(saved_log_path+'CV_AND_Clinical_Training_NO_FS_400_LogisticRegression_c.npy',best_c_training_400_cli_and_cv)
		# np.save(saved_log_path+'CV_AND_Clinical_Training_WITH_FS_400_LogisticRegression_c.npy',best_c_training_400_cli_and_cv_with_fs)
		uni_param_set_cli_and_cv,counts_param_set_cli_and_cv = np.unique(best_param_set_training_400_cli_and_cv,return_counts = True)
		most_selected_param_set_cv_and_cli = uni_param_set_cli_and_cv[np.flipud(np.argsort(counts_param_set_cli_and_cv))][0]
		print('Most Selected Parameter Set for Radiomics+Clinical: ', most_selected_param_set_cv_and_cli)
		np.save(saved_log_path+'CV_AND_Clinical_Training_WITH_FS_400_LogisticRegression_picked_features_idx.npy',idx_feat_picked_training_400)
		##
		f_feat_picked = open(saved_log_path + 'Clinical_not_categorical_and_CV_Training_WITH_FS_400_LogisticRegression_picked_features_names.txt','w')
		for one_select in feat_picked_training_400:
			f_feat_picked.write(' + '.join(one_select)+'\n')
		f_feat_picked.close()
		##
		f_log.close()
		f_csv_cv_and_cli_labels.close()
		f_csv_cv_and_cli_nofs.close()
		f_csv_cv_and_cli_with_fs.close()
		####################### FINAL Testing, On Training Set#######################
		# info_training_400
		############################ AUC Performance On TEST ############################
		## Model A + No FS on Clinical Features
		# uni_param_set_cli,counts_param_set_cli = np.unique(best_param_set_training_400_cli,return_counts = True)
		# most_selected_param_set_cli = uni_param_set_cli[np.flipud(np.argsort(counts_param_set_cli))][0]
		# svm_cli_with_best_param_set = svm.SVC(C = most_selected_param_set_cli['C'], kernel = most_selected_param_set_cli['kernel'], gamma = most_selected_param_set_cli['gamma'], probability = True, max_iter = 20000)
		# _=svm_cli_with_best_param_set.fit(X_training_400_clinical,Y_training_400)	
		# proba_test_300_cli = svm_cli_with_best_param_set.predict_proba(X_testing_300_clinical)[:,1]
		# auc_test_300_clinical = metrics.roc_auc_score(Y_testing_300,proba_test_300_cli)
		## Model A + No FS on CV
		uni_param_set_cv,counts_param_set_cv = np.unique(best_param_set_training_400_cv,return_counts = True)
		most_selected_param_set_cv = uni_param_set_cv[np.flipud(np.argsort(counts_param_set_cv))][0]
		svm_cv_with_best_param_set = svm.SVC(C = most_selected_param_set_cv['C'], kernel = most_selected_param_set_cv['kernel'], gamma = most_selected_param_set_cv['gamma'], probability = True, max_iter = 20000)
		_ = svm_cv_with_best_param_set.fit(X_training_400_radiomics,Y_training_400)
		proba_test_300_cv = svm_cv_with_best_param_set.predict_proba(X_testing_300_radiomics)[:,1]
		auc_test_300_radiomics_nofs = metrics.roc_auc_score(Y_testing_300,proba_test_300_cv)
		## Model A Top Features
		selected_features_cv_only_num = 11
		# idx_feat_picked_modelA_400 = np.load(saved_log_path+'CV_ONLY_Training_WITH_FS_400_LogisticRegression_picked_features_idx.npy')
		# flatten_idx_feat_picked_modelA_400 = []
		# for one_select in idx_feat_picked_modelA_400:
		#   flatten_idx_feat_picked_modelA_400.extend(one_select)

		# uni_feat_cv,counts_feat_cv = np.unique(flatten_idx_feat_picked_modelA_400,return_counts = True)
		# sorted_feat_cv = uni_feat_cv[np.flipud(np.argsort(counts_feat_cv))]
		# sorted_counts_cv = np.flipud(np.sort(counts_feat_cv))
		# X_training_400_radiomics_subset = np.copy(X_training_400_radiomics[:,sorted_feat_cv[:selected_features_cv_only_num]])
		# X_testing_300_radiomics_subset = np.copy(X_testing_300_radiomics[:,sorted_feat_cv[:selected_features_cv_only_num]])
		# ##
		# ## Model A + Top Feats on CV 
		# uni_param_set_cv_topfeats,counts_param_set_cv_topfeats = np.unique(best_param_set_training_400_cv_topfeats,return_counts = True)
		# most_selected_param_set_cv_topfeats = uni_param_set_cv_topfeats[np.flipud(np.argsort(counts_param_set_cv_topfeats))][0]
		# svm_cv_topfeats_with_best_param_set = svm.SVC(C = most_selected_param_set_cv_topfeats['C'], kernel = most_selected_param_set_cv_topfeats['kernel'], gamma = most_selected_param_set_cv_topfeats['gamma'], probability = True, max_iter = 20000)
		# _ = svm_cv_topfeats_with_best_param_set.fit(X_training_400_radiomics_subset,Y_training_400)
		# proba_test_300_cv_topfeats = svm_cv_topfeats_with_best_param_set.predict_proba(X_testing_300_radiomics_subset)[:,1]
		# auc_test_300_radiomics_topfeats = metrics.roc_auc_score(Y_testing_300,proba_test_300_top_feats) 
		## Model A + No FS on CV + Clinical
		uni_param_set_cli_and_cv,counts_param_set_cli_and_cv = np.unique(best_param_set_training_400_cli_and_cv,return_counts = True)
		most_selected_param_set_cv_and_cli = uni_param_set_cli_and_cv[np.flipud(np.argsort(counts_param_set_cli_and_cv))][0]
		svm_cv_and_cli_with_best_param_set = svm.SVC(C = most_selected_param_set_cv_and_cli['C'], kernel = most_selected_param_set_cv_and_cli['kernel'], gamma = most_selected_param_set_cv_and_cli['gamma'], probability = True, max_iter = 20000)
		_ = svm_cv_and_cli_with_best_param_set.fit(X_training_400_radiomics_and_clinical,Y_training_400)
		proba_test_300_cv_and_cli = svm_cv_and_cli_with_best_param_set.predict_proba(X_testing_300_radiomics_and_clinical)[:,1]
		auc_test_300_radiomics_and_clinical = metrics.roc_auc_score(Y_testing_300,proba_test_300_cv_and_cli) 
		################
		with open(all_train_test_results_per_shuffle, 'a') as f_train_test_results_reshuffled:
			rea_train_test_results_reshuffled = csv.writer(f_train_test_results_reshuffled)
			# rea_train_test_results_reshuffled.writerow([random_seed_for_train_test_reshuffle, p_age, p_lesion, auc_training_400_cv_nofs.mean(),auc_training_400_cli_and_cv_nofs.mean(), auc_training_400_cli_and_cv_with_fs.mean(), auc_test_300_radiomics_nofs, auc_test_300_radiomics_and_clinical])
			rea_train_test_results_reshuffled.writerow([random_seed_for_train_test_reshuffle, 0,0, auc_training_400_cv_nofs.mean(),auc_training_400_cli_and_cv_nofs.mean(), auc_training_400_cli_and_cv_with_fs.mean(), auc_test_300_radiomics_nofs, auc_test_300_radiomics_and_clinical])
		################
		print('****************##########************* Final Results ****************##########*************')
		print('Random Seed\tTraining Radiomics Features Only AUC\tTraining Radiomics + Clinical Features AUC\tTesting Radiomics Features Only AUC\tTesting Radiomics + Clinical Features AUC')
		print('%d\t%.3f\t%.3f\t%.3f\t%.3f' \
		%(random_seed_for_train_test_reshuffle, auc_training_400_cv_nofs.mean(), auc_training_400_cli_and_cv_nofs.mean(), \
			auc_test_300_radiomics_nofs, auc_test_300_radiomics_and_clinical ))
		print('****************##########************* DONE ONE SHUFFLING ****************##########*************')
		print('****************##########************* DONE ONE SHUFFLING ****************##########*************')
		##
		with open(saved_log_path + 'Final_SVM_radiomics_pred_and_labels_3models_seed%d_{CV-CV+CLI}_{%.3f, %.3f}.csv'\
			%(random_seed_for_train_test_reshuffle, auc_test_300_radiomics_nofs, auc_test_300_radiomics_and_clinical),'w') as f_csv_final_test300:
			wri_csv_final_test300 = csv.writer(f_csv_final_test300)
			_= wri_csv_final_test300.writerow(['ID','labels','pred.CV','pred.CV.plus.clinical'])
			_=[wri_csv_final_test300.writerow([names_testing_300[idss],Y_testing_300[idss], proba_test_300_cv[idss], proba_test_300_cv_and_cli[idss]]) for idss in range(Y_testing_300.shape[0])]
		##

