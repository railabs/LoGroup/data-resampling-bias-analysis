## python -W ignore ..py
## Created Date: August 28, 2023
## Name: Rui Hou
## Study: Data Resampling Bias
## Requirement: Python 2.7

import sys,os,random,math,time,glob,csv,scipy.io,copy,xlrd,datetime,xlwt
import numpy as np
from scipy import interp
# import matplotlib.pyplot as plt
from itertools import cycle
from sklearn import svm, metrics, linear_model,preprocessing
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import RandomizedLogisticRegression
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline
from functools import reduce
from sklearn.feature_selection import RFE
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 
warnings.filterwarnings("ignore", category=RuntimeWarning)
warnings.filterwarnings("ignore", category=FutureWarning)

def mean_std_normalize(X_new_tr,X_new_val):
	mean_std_scaler0 = preprocessing.StandardScaler(copy=True, with_mean=True, with_std=True)
	X_tr = mean_std_scaler0.fit_transform(X_new_tr)
	if len(X_new_val)>0:
		X_val = mean_std_scaler0.transform(X_new_val)
	else:
		X_val = []
	return X_tr,X_val


if __name__ == '__main__':
	# num_reshuffle_times = int(sys.argv[1]) #60 
	one_shuffle_time = int(sys.argv[1])
	iterTimes = int(sys.argv[2]) #200 
	num_cpus_to_load = int(sys.argv[3])
	####
	cluster_features_names = ['MCC Area', 'MCC Eccentricity', 'MCC NO of MCs',  'MCC MCs Coverage', 'MCC Background Mean', 'MCC Background Std', 'MCC All MCs Intensity Mean', 'MCC All MCs Intensity Std', 'Mean GLCM Contrast', 'Mean GLCM Correlation', 'Mean GLCM Energy', 'Mean GLCM Homogeneity', 'Lesion Size']
	ind_features_names = ['MC Perimeter', 'MC Area', 'MC Circularity', 'MC Eccentricity', 'MC Major Axis', 'MC Minor Axis', '1st Variant of Hu Moments','2nd Variant of Hu Moments','3rd Variant of Hu Moments','4th Variant of Hu Moments','5th Variant of Hu Moments','6th Variant of Hu Moments','7th Variant of Hu Moments','MC Distance to Cluster Centroid','MC Distance to Nearest MC','MC Normalized Degree','MC Intensity Mean','MC Intensity Std','MC Background Intensity Mean','MC Background Intensity Std','MC Mean GLCM Contrast','MC Mean GLCM Correlation','MC Mean GLCM Energy','MC Mean GLCM Homogeneity']
	radiomics_feat_list =	cluster_features_names[:-1] + [' '.join(['Mean',ss]) for ss in ind_features_names] + [' '.join(['Std',ss]) for ss in ind_features_names] + \
						[' '.join(['Min',ss]) for ss in ind_features_names] + [' '.join(['Max',ss]) for ss in ind_features_names] + \
						cluster_features_names[-1:]

	clinical_feat_list = ['Nuclear Grade','ER','PR','Age']
	####
	num_radiomic_features = len(radiomics_feat_list)
	num_clinical_features = 4
	##***
	r_csv_path = 'normalized_features_csv/'
	num_of_features_cv = 109
	f_train_dcis400_cv_and_cli = open(r_csv_path + 'Train_400_features_CV_and_CLINICAL.csv','r')
	f_test_dcis300_cv_and_cli = open(r_csv_path + 'Test_300_features_CV_and_CLINICAL.csv','r')

	rea_dcis400_cv_and_cli = csv.reader(f_train_dcis400_cv_and_cli)
	header_dcis400_cv_and_cli = next(rea_dcis400_cv_and_cli)
	feat_only_all_dcis400_cv_and_cli = []
	for one_row in rea_dcis400_cv_and_cli:
		feat_only_all_dcis400_cv_and_cli.append(one_row)

	f_train_dcis400_cv_and_cli.close()
	###
	rea_dcis300_cv_and_cli = csv.reader(f_test_dcis300_cv_and_cli)
	header_dcis300_cv_and_cli = next(rea_dcis300_cv_and_cli)
	feat_only_all_dcis300_cv_and_cli = []
	for one_row in rea_dcis300_cv_and_cli:
		feat_only_all_dcis300_cv_and_cli.append(one_row)

	f_test_dcis300_cv_and_cli.close()

	num_of_features_cv_and_cli = len(header_dcis400_cv_and_cli)-2
	num_of_features_cli = num_of_features_cv_and_cli - num_of_features_cv
	##
	new_feat_list_cv_and_cli = [ii.replace('.',' ') for ii in header_dcis400_cv_and_cli[2:2+num_of_features_cv_and_cli]]
	new_feat_list_cv = [ii.replace('.',' ') for ii in header_dcis400_cv_and_cli[2:2+num_of_features_cv]]
	new_feat_list_cli = [ii.replace('.',' ') for ii in header_dcis400_cv_and_cli[2+num_of_features_cv:]]
	########################################################################################################################################
	idx_dx = [idss for idss,he in enumerate(header_dcis400_cv_and_cli) if he=='Label'][0]
	X_training_400_radiomics_and_clinical = np.array([]).reshape(0,num_of_features_cv_and_cli)
	Y_training_400 = np.array([]).astype(np.uint8)
	for idss,one_row in enumerate(feat_only_all_dcis400_cv_and_cli):
		one_feat_now = np.array([float(ii) for ii in one_row[2:num_of_features_cv_and_cli+2]]).reshape((1,num_of_features_cv_and_cli))
		one_dx_now = np.array([int(one_row[idx_dx])])
		X_training_400_radiomics_and_clinical = np.concatenate((X_training_400_radiomics_and_clinical,one_feat_now),axis = 0)
		Y_training_400 = np.concatenate((Y_training_400,one_dx_now),axis = 0)

	########################################################################################################################################
	idx_dx = [idss for idss,he in enumerate(header_dcis300_cv_and_cli) if he=='Label'][0]
	X_testing_300_radiomics_and_clinical = np.array([]).reshape(0,num_of_features_cv_and_cli)
	Y_testing_300 = np.array([]).astype(np.uint8)
	for idss,one_row in enumerate(feat_only_all_dcis300_cv_and_cli):
		one_feat_now = np.array([float(ii) for ii in one_row[2:num_of_features_cv_and_cli+2]]).reshape((1,num_of_features_cv_and_cli))
		one_dx_now = np.array([int(one_row[idx_dx])])
		X_testing_300_radiomics_and_clinical = np.concatenate((X_testing_300_radiomics_and_clinical,one_feat_now),axis = 0)
		Y_testing_300 = np.concatenate((Y_testing_300,one_dx_now),axis = 0)

	X_training_400_radiomics_and_clinical = X_training_400_radiomics_and_clinical.astype(np.float32)
	X_testing_300_radiomics_and_clinical = X_testing_300_radiomics_and_clinical.astype(np.float32)

	###########################################################################################
	X_all_radiomics_and_clinical = np.concatenate((X_training_400_radiomics_and_clinical, X_testing_300_radiomics_and_clinical), axis = 0)
	X_all_radiomics = np.concatenate((X_training_400_radiomics, X_testing_300_radiomics), axis = 0)
	X_all_clinical = np.concatenate((X_training_400_clinical, X_testing_300_clinical), axis = 0)
	Y_all = np.concatenate((Y_training_400, Y_testing_300), axis = 0)
	all_lesion_age = np.array(training_lesion_age + testing_lesion_age)
	########################
	nfold = 5
	embedded_cv = 4
	pow_base_c = 10
	c_min = -8
	c_max = 8
	Cs = [pow(pow_base_c,i) for i in range(c_min,c_max)]
	param_grid = {'C': Cs}
	#####
	saved_log_path  = '/home/rh163/Documents/DCIS_New_Study/results/Finalized_DCIS_v1_to_5_all/' + \
						'training_results/results_norm_independently/reshuffle_and_add_cases/'
	if not os.path.isdir(saved_log_path):
		os.mkdir(saved_log_path)
	###
	csv_file_name = saved_log_path + 'Different_training_cases_and_shuffles_aucs.csv'
	if not os.path.isfile(csv_file_name):
		with open(csv_file_name, 'w') as f_csv:
			wri = csv.writer(f_csv)
			wri.writerow(['Shuffle Times', 'Random Seed', 'Number of Training Cases', 'Age', 'Lesion','Radiomics Only AUC', 'Radiomics Plus Clinical AUC'])

	########################
	# random_list = random.sample(range(1,1000), num_reshuffle_times*2)
	# random_list_subset = np.unique(random_list)[:num_reshuffle_times]
	random_list_subset = [63, 894, 983, 395, 235, 950, 195, 917, 242, 820, 956, 802, 886, 755, 211, 106, 741, 952, 539, 879, 58, 915, 397, 148, 423, 862, 191, 854, 848, 394, 248, 69, 311, 667, 36, 609, 154, 111, 553, 827, 80, 347, 600, 388, 45, 97, 843, 792, 16, 538, 273, 367, 798, 192, 641, 126, 473, 914, 982, 121, 208, 315, 9, 658, 124, 536, 788, 839, 697, 33, 61, 611, 508, 103, 259, 396, 356, 585, 175, 677, 775, 54, 318, 30, 454, 947, 339, 44, 168, 870, 812, 989, 720, 566, 455, 31, 464]
	##
	ind0_all = [indsub for indsub,x in enumerate(Y_all) if x==0]
	ind1_all = [indsub for indsub,x in enumerate(Y_all) if x==1]
	upstage_ratio = float(len(ind1_all))/float(len(ind0_all))
	# for one_shuffle_time, one_shuffle_seed in enumerate(random_list_subset):
	one_shuffle_seed = random_list_subset[one_shuffle_time]
	##
	random.seed(one_shuffle_seed);random.shuffle(ind0_all);random.shuffle(ind1_all)
	for subset_case_numbsers in range(100,700,100):
		f_log = open(saved_log_path + 'logs_%dCases_%dSeed_radiomics_and_radiomics_plus_clinical.log'%(subset_case_numbsers, one_shuffle_seed),'a')
		num_training_upstaging = int(upstage_ratio*subset_case_numbsers) 
		num_training_pure = subset_case_numbsers - num_training_upstaging
		ind1_now = ind1_all[ :num_training_upstaging]
		ind0_now = ind0_all[ :num_training_pure]
		##
		lesion_age_now = all_lesion_age[np.array(ind1_now + ind0_now), :]
		mean_lesion_age = lesion_age_now.mean(0)
		f_log.write('Current Mean Age and Lesion: %.3f\t%.3f\n'%(round(mean_lesion_age[0], 3), round(mean_lesion_age[1], 3)))
		##
		num_holdout_pure = int(round(num_training_pure/float(nfold)))
		num_holdout_upstaging = int(round(num_training_upstaging/float(nfold)))
		auc_radiomics_nofs = np.zeros([iterTimes,nfold])
		auc_radiomics_and_clinical_nofs = np.zeros([iterTimes,nfold])
		for iter2,seed_val in zip(range(iterTimes),range(iterTimes)):	
			random.shuffle(ind0_now); random.shuffle(ind1_now)
			for nf in range(nfold):
				ind_val = ind0_now[nf*num_holdout_pure:nf*num_holdout_pure+num_holdout_pure]+ind1_now[nf*num_holdout_upstaging:nf*num_holdout_upstaging+num_holdout_upstaging]
				ind_tr = list(set(ind0_now+ind1_now)-set(ind_val))
				Y_val = np.array(Y_all[np.array(ind_val)])
				Y_tr = np.array(Y_all[np.array(ind_tr)])
				X_tr = np.copy(X_all_radiomics[np.array(ind_tr),:])
				X_val = np.copy(X_all_radiomics[np.array(ind_val),:])
				lr_radiomics_infold = GridSearchCV(linear_model.LogisticRegression(penalty='l2',solver='lbfgs',max_iter = 20000), param_grid, cv=embedded_cv,scoring='roc_auc', n_jobs = num_cpus_to_load)
				_=lr_radiomics_infold.fit(X_tr,Y_tr)
				param_set_training_400_cv_now = lr_radiomics_infold.best_params_
				val_proba_now = lr_radiomics_infold.predict_proba(X_val)[:,1]
				auc_radiomics_nofs[iter2,nf] = metrics.roc_auc_score(Y_val,val_proba_now)
				##
				X_tr_rc = np.copy(X_all_radiomics_and_clinical[np.array(ind_tr),:])
				X_val_rc = np.copy(X_all_radiomics_and_clinical[np.array(ind_val),:])
				lr_radiomics_and_clinical_infold = GridSearchCV(linear_model.LogisticRegression(penalty='l2',solver='lbfgs',max_iter = 20000), param_grid, cv=embedded_cv,scoring='roc_auc', n_jobs = num_cpus_to_load)
				_=lr_radiomics_and_clinical_infold.fit(X_tr_rc,Y_tr)
				val_proba_now_rc = lr_radiomics_and_clinical_infold.predict_proba(X_val_rc)[:,1]
				auc_radiomics_and_clinical_nofs[iter2,nf] = metrics.roc_auc_score(Y_val,val_proba_now_rc)
			print('------Run %d With %d Cases Radiomics Only AUC: %0.3f +- %0.3f; Radiomics PLUS Clinical AUC: %0.3f +- %0.3f' \
				%(iter2+1, subset_case_numbsers, round(auc_radiomics_nofs[:iter2+1].mean(),3),round(auc_radiomics_nofs[:iter2+1].mean(1).std(),3), \
				round(auc_radiomics_and_clinical_nofs[:iter2+1].mean(),3),round(auc_radiomics_and_clinical_nofs[:iter2+1].mean(1).std(),3)) )
			f_log.write('------Run %d With %d Cases Radiomics Only AUC: %0.3f +- %0.3f; Radiomics PLUS Clinical AUC: %0.3f +- %0.3f\n' \
				%(iter2+1, subset_case_numbsers,   round(auc_radiomics_nofs[:iter2+1].mean(),3),round(auc_radiomics_nofs[:iter2+1].mean(1).std(),3), \
				round(auc_radiomics_and_clinical_nofs[:iter2+1].mean(),3),round(auc_radiomics_and_clinical_nofs[:iter2+1].mean(1).std(),3)) )
		print('*** %d Cases (Shuffle Times: %d )Radiomics Only AUC: %0.3f +- %0.3f; Radiomics PLUS Clinical AUC: %0.3f +- %0.3f\n' \
			%(subset_case_numbsers, one_shuffle_time+1, round(auc_radiomics_nofs.mean(),3),round(auc_radiomics_nofs.mean(1).std(),3),\
				round(auc_radiomics_and_clinical_nofs.mean(),3),round(auc_radiomics_and_clinical_nofs.mean(1).std(),3)))
		###
		f_log.write('*** %d Cases (Shuffle Times: %d, seed: %d )Radiomics Only AUC: %0.3f +- %0.3f; Radiomics PLUS Clinical AUC: %0.3f +- %0.3f\n' \
			%(subset_case_numbsers, one_shuffle_time+1, one_shuffle_seed, round(auc_radiomics_nofs.mean(),3),round(auc_radiomics_nofs.mean(1).std(),3),\
				round(auc_radiomics_and_clinical_nofs.mean(),3),round(auc_radiomics_and_clinical_nofs.mean(1).std(),3)))
		f_log.close()
		with open(csv_file_name, 'a') as f_csv:
			wri = csv.writer(f_csv)
			wri.writerow([one_shuffle_time + 1, one_shuffle_seed, subset_case_numbsers, mean_lesion_age[0], mean_lesion_age[1], round(auc_radiomics_nofs.mean(),3),round(auc_radiomics_and_clinical_nofs.mean(),3) ])
	