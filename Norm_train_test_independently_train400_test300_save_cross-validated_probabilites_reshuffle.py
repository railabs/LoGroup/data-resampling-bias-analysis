## python -W ignore ..py
import sys,os,random,math,time,glob,csv,scipy.io,copy,xlrd,datetime,xlwt,dicom,pydicom,cv2
import numpy as np
from scipy import interp
# import matplotlib.pyplot as plt
from itertools import cycle
from sklearn import svm, metrics, linear_model,preprocessing
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import RandomizedLogisticRegression
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from skimage.morphology import convex_hull_image
from skimage.measure import regionprops,label
from skimage.feature.texture import greycomatrix,greycoprops
from scipy.spatial.distance import pdist,cdist
from scipy.misc import imread,imsave

import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 
warnings.filterwarnings("ignore", category=RuntimeWarning)
warnings.filterwarnings("ignore", category=FutureWarning)

def mean_std_normalize(X_new_tr,X_new_val):
	mean_std_scaler0 = preprocessing.StandardScaler(copy=True, with_mean=True, with_std=True)
	X_tr = mean_std_scaler0.fit_transform(X_new_tr)
	if len(X_new_val)>0:
		X_val = mean_std_scaler0.transform(X_new_val)
	else:
		X_val = []
	return X_tr,X_val


if __name__ == '__main__':
	num_reshuffle_times = int(sys.argv[1]) #60 
	iterTimes = int(sys.argv[2]) #200 
	num_cpus_to_load = 20
	all_dcm_roi_root_path = 'ALL_DCM_ROI/'
	####
	cluster_features_names = ['MCC Area', 'MCC Eccentricity', 'MCC NO of MCs',  'MCC MCs Coverage', 'MCC Background Mean', 'MCC Background Std', 'MCC All MCs Intensity Mean', 'MCC All MCs Intensity Std', 'Mean GLCM Contrast', 'Mean GLCM Correlation', 'Mean GLCM Energy', 'Mean GLCM Homogeneity', 'Lesion Size']
	ind_features_names = ['MC Perimeter', 'MC Area', 'MC Circularity', 'MC Eccentricity', 'MC Major Axis', 'MC Minor Axis', '1st Variant of Hu Moments','2nd Variant of Hu Moments','3rd Variant of Hu Moments','4th Variant of Hu Moments','5th Variant of Hu Moments','6th Variant of Hu Moments','7th Variant of Hu Moments','MC Distance to Cluster Centroid','MC Distance to Nearest MC','MC Normalized Degree','MC Intensity Mean','MC Intensity Std','MC Background Intensity Mean','MC Background Intensity Std','MC Mean GLCM Contrast','MC Mean GLCM Correlation','MC Mean GLCM Energy','MC Mean GLCM Homogeneity']
	radiomics_feat_list =	cluster_features_names[:-1] + [' '.join(['Mean',ss]) for ss in ind_features_names] + [' '.join(['Std',ss]) for ss in ind_features_names] + \
						[' '.join(['Min',ss]) for ss in ind_features_names] + [' '.join(['Max',ss]) for ss in ind_features_names] + \
						cluster_features_names[-1:]

	clinical_feat_list = ['Nuclear Grade','ER','PR','Age']
	####
	num_radiomic_features = len(radiomics_feat_list)
	num_clinical_features = 4
	##***
	xlsx_file_name_alldcis = 'original_file.xlsx'
	train_and_test_names_with_dx_model_cli_feat_all = 
	####################
	dcis_file = xlrd.open_workbook(xlsx_file_name_alldcis)
	dcis_list_all = dcis_file.sheet_by_index(0)
	dcis_training= dcis_file.sheet_by_index(1)
	dcis_testing = dcis_file.sheet_by_index(2)

	#########DCIS
	random_list_subset = [374, 46, 50, 383, 398, 411, 419, 421, 437, 441, 824, 652, 588, 312, 453, 454, 467, 909, 210, 208, 468, 472, 475, 479, 489, 498, 511, 577, 624, 935, 76, 334, 34, 43, 445, 449, 492]
	for reshuffle_ids_now in range(num_reshuffle_times):
		random_seed_for_train_test_reshuffle = random_list_subset[reshuffle_ids_now]# random.randint(1,1000)
		##
		normalized_train_test_csv_saved_folder = 'random_shuffling_seed%d/' %(random_seed_for_train_test_reshuffle)
		saved_log_path  = '/training_results/random_shuffling_seed%d/' %(random_seed_for_train_test_reshuffle)

		random.seed(random_seed_for_train_test_reshuffle); random.shuffle(train_and_test_names_with_dx_model_cli_feat_all)
		##

		#####################################################################################
		train_normalized_radiomics_clinical_file_name = normalized_train_test_csv_saved_folder + 'Train_400_features_CV_and_CLINICAL_' + xlsx_file_name_alldcis.split('/')[-1].replace('.xlsx','.csv')
		test_normalized_radiomics_clinical_file_name = normalized_train_test_csv_saved_folder + 'Test_300_features_CV_and_CLINICAL_' + xlsx_file_name_alldcis.split('/')[-1].replace('.xlsx','.csv')
		##
		X_training_400_radiomics_and_clinical = np.array([]).reshape(0, num_radiomic_features + num_clinical_features)
		Y_training_400 = np.array([])
		with open(train_normalized_radiomics_clinical_file_name,'r') as f_feat_training_400_cli_and_cv:
			rea_training_400_cli_and_cv = csv.reader(f_feat_training_400_cli_and_cv)
			header_training_400_cli_and_cv = next(rea_training_400_cli_and_cv)
			for one_row in rea_training_400_cli_and_cv:
				one_dx = [int(one_row[1])]
				Y_training_400 = np.concatenate((Y_training_400,one_dx))
				X_one_feat = np.array(one_row[2:]).reshape(1, num_radiomic_features + num_clinical_features)
				X_training_400_radiomics_and_clinical = np.concatenate((X_training_400_radiomics_and_clinical, X_one_feat), axis = 0)

		X_training_400_radiomics = X_training_400_radiomics_and_clinical[:, :num_radiomic_features]
		X_training_400_clinical = X_training_400_radiomics_and_clinical[:, num_radiomic_features: ]
		######
		names_testing_300 = []
		X_testing_300_radiomics_and_clinical = np.array([]).reshape(0, num_radiomic_features + num_clinical_features)
		Y_testing_300 = np.array([])
		with open(test_normalized_radiomics_clinical_file_name,'r') as f_feat_testing_300_cli_and_cv:
			rea_testing_300_cli_and_cv = csv.reader(f_feat_testing_300_cli_and_cv)
			header_testing_300_cli_and_cv = next(rea_testing_300_cli_and_cv)
			for one_row in rea_testing_300_cli_and_cv:
				names_testing_300.append(str(one_row[0]))
				one_dx = [int(one_row[1])]
				Y_testing_300 = np.concatenate((Y_testing_300,one_dx))
				X_one_feat = np.array(one_row[2:]).reshape(1, num_radiomic_features + num_clinical_features)
				X_testing_300_radiomics_and_clinical = np.concatenate((X_testing_300_radiomics_and_clinical, X_one_feat), axis = 0)

		X_testing_300_radiomics = X_testing_300_radiomics_and_clinical[:, :num_radiomic_features]
		X_testing_300_clinical = X_testing_300_radiomics_and_clinical[:, num_radiomic_features: ]

		###########################################################################################
		nfold = 5
		embedded_cv = 4
		pow_base_c = 10
		c_min = -10
		c_max = 10
		Cs = [pow(pow_base_c,i) for i in range(c_min,c_max)]
		param_grid = {'C': Cs}
		#####
		####################################################With ALL Clinical Features Only###########################################################
		f_log = open(saved_log_path + 'logs_FOUR_CLINICAL_Features_Only_save_predictions_LogisticRegression.log','a')
		best_c_training_400_cli = []
		auc_training_400_cli_nofs = np.zeros([iterTimes,nfold])
		##
		num_training_pure = np.where(Y_training_400==0)[0].shape[0]
		num_training_upstaging = np.where(Y_training_400==1)[0].shape[0]
		num_holdout_pure = int(round(num_training_pure/float(nfold)))
		num_holdout_upstaging = int(round(num_training_upstaging/float(nfold)))
		##
		f_csv_cli_only_labels = open(saved_log_path + 'CLINICAL_FOUR_only_Labels_with_%dfolds_%diterations.csv'%(nfold,iterTimes),'w')
		wri_cli_only_labels = csv.writer(f_csv_cli_only_labels)
		f_csv_cli_only_nofs = open(saved_log_path + 'CLINICAL_FOUR_only_Predictions_with_%dfolds_%diterations.csv'%(nfold,iterTimes),'w')
		wri_cli_only_nofs = csv.writer(f_csv_cli_only_nofs)
		##
		ind0 = [indsub for indsub,x in enumerate(Y_training_400) if x==0]
		ind1 = [indsub for indsub,x in enumerate(Y_training_400) if x==1]
		for iter2,seed_val in zip(range(iterTimes),range(iterTimes)):	
			random.seed((iterTimes-seed_val)*2);random.shuffle(ind0)	
			random.seed((iterTimes-seed_val)*2);random.shuffle(ind1)
			for nf in range(nfold):
				ind_val = ind0[nf*num_holdout_pure:nf*num_holdout_pure+num_holdout_pure]+ind1[nf*num_holdout_upstaging:nf*num_holdout_upstaging+num_holdout_upstaging]
				ind_tr = list(set(ind0+ind1)-set(ind_val))
				X_new_tr = np.copy(X_training_400_clinical[np.array(ind_tr),:])
				Y_tr = np.array(Y_training_400[np.array(ind_tr)])
				X_new_val = np.copy(X_training_400_clinical[np.array(ind_val),:])
				Y_val = np.array(Y_training_400[np.array(ind_val)])
				wri_cli_only_labels.writerow(Y_val.tolist())
				X_tr = np.copy(X_new_tr);X_val = np.copy(X_new_val)
				lr_baseline_infold = GridSearchCV(linear_model.LogisticRegression(penalty='l2',solver='lbfgs',max_iter = 20000), param_grid, cv=embedded_cv,scoring='roc_auc', n_jobs = num_cpus_to_load)
				_=lr_baseline_infold.fit(X_tr,Y_tr)
				best_c_now = lr_baseline_infold.best_params_['C']
				best_c_training_400_cli.extend([best_c_now])
				val_proba_now = lr_baseline_infold.predict_proba(X_val)[:,1]
				wri_cli_only_nofs.writerow(val_proba_now.tolist())
				auc_training_400_cli_nofs[iter2,nf] = metrics.roc_auc_score(Y_val,val_proba_now)
			print('--------- Run %d With Four Clinical Features AUC: %0.3f +- %0.3f' %(iter2+1, round(auc_training_400_cli_nofs[:iter2+1].mean(),3),round(auc_training_400_cli_nofs[:iter2+1].mean(1).std(),3)))
			f_log.write('--------- Run %d With Four Clinical Features AUC: %0.3f +- %0.3f\n' %(iter2+1, round(auc_training_400_cli_nofs[:iter2+1].mean(),3),round(auc_training_400_cli_nofs[:iter2+1].mean(1).std(),3)))
		print('--- With Four Clinical Features AUC: %0.3f +- %0.3f' %(round(auc_training_400_cli_nofs[:iter2+1].mean(),3),round(auc_training_400_cli_nofs[:iter2+1].mean(1).std(),3)))
		f_log.write('--- With Four Clinical Features AUC: %0.3f +- %0.3f\n' %(round(auc_training_400_cli_nofs[:iter2+1].mean(),3),round(auc_training_400_cli_nofs[:iter2+1].mean(1).std(),3)))
		np.save(saved_log_path + 'CLINICAL_FOUR_only_Training_NO-FS_best_parameters_C.npy',best_c_training_400_cli)
		f_log.close()
		################################CV ONLY Logistic Regression with Feature Selection ######################
		f_log = open(saved_log_path + 'logs_CV_ONLY_LogisticRegression_WITH_and_WITHOUT_feature_selection_with_saving_predictions.log','a')
		## Model A on 400 Training Data with Logistic Regression
		num_training_pure = np.where(Y_training_400==0)[0].shape[0]
		num_training_upstaging = np.where(Y_training_400==1)[0].shape[0]
		num_holdout_pure = int(round(num_training_pure/float(nfold)))
		num_holdout_upstaging = int(round(num_training_upstaging/float(nfold)))
		##
		f_csv_cv_labels = open(saved_log_path + 'CV_ONLY_Labels_with_%dfolds_%diterations.csv'%(nfold,iterTimes),'w')
		wri_cv_labels = csv.writer(f_csv_cv_labels)
		##
		best_c_training_400_cv = []
		auc_training_400_cv_nofs = np.zeros([iterTimes,nfold])
		f_csv_cv_nofs = open(saved_log_path + 'CV_ONLY_NO-FS_Predictions_with_%dfolds_%diterations.csv'%(nfold,iterTimes),'w')
		wri_cv_nofs = csv.writer(f_csv_cv_nofs)
		##
		best_c_training_400_cv_with_fs = []
		auc_training_400_cv_with_fs = np.zeros([iterTimes,nfold])
		f_csv_cv_with_fs = open(saved_log_path + 'CV_ONLY_WITH-FS_Predictions_with_%dfolds_%diterations.csv'%(nfold,iterTimes),'w')
		wri_cv_with_fs = csv.writer(f_csv_cv_with_fs)
		##
		idx_feat_picked_training_400 = []
		feat_picked_training_400 = []
		ind0 = [indsub for indsub,x in enumerate(Y_training_400) if x==0]
		ind1 = [indsub for indsub,x in enumerate(Y_training_400) if x==1]
		for iter2,seed_val in zip(range(iterTimes),range(iterTimes)):	
			random.seed((iterTimes-seed_val)*2);random.shuffle(ind0)	
			random.seed((iterTimes-seed_val)*2);random.shuffle(ind1)
			for nf in range(nfold):
				ind_val = ind0[nf*num_holdout_pure:nf*num_holdout_pure+num_holdout_pure]+ind1[nf*num_holdout_upstaging:nf*num_holdout_upstaging+num_holdout_upstaging]
				ind_tr = list(set(ind0+ind1)-set(ind_val))
				X_new_tr = np.copy(X_training_400_radiomics[np.array(ind_tr),:])
				Y_tr = np.array(Y_training_400[np.array(ind_tr)])
				X_new_val = np.copy(X_training_400_radiomics[np.array(ind_val),:])
				Y_val = np.array(Y_training_400[np.array(ind_val)])
				wri_cv_labels.writerow(Y_val.tolist())
				X_tr = np.copy(X_new_tr);X_val = np.copy(X_new_val)
				lr_baseline_infold = GridSearchCV(linear_model.LogisticRegression(penalty='l2',solver='lbfgs',max_iter = 20000), param_grid, cv=embedded_cv,scoring='roc_auc', n_jobs = num_cpus_to_load)
				_=lr_baseline_infold.fit(X_tr,Y_tr)
				best_c_now = lr_baseline_infold.best_params_['C']
				best_c_training_400_cv.extend([best_c_now])
				val_proba_now = lr_baseline_infold.predict_proba(X_val)[:,1]
				wri_cv_nofs.writerow(val_proba_now.tolist())
				auc_training_400_cv_nofs[iter2,nf] = metrics.roc_auc_score(Y_val,val_proba_now)
				##
				randomized_logistic = RandomizedLogisticRegression(C=1,random_state=nfold)
				randomized_logistic.fit(X_tr,Y_tr)
				ind_fea = randomized_logistic.all_scores_[:,0]
				rank_one_ind_fea = np.flipud(np.argsort(ind_fea))
				current_idx_feat_picked = [i for i in rank_one_ind_fea if ind_fea[i]>0]
				if len(current_idx_feat_picked) > 0:
					current_feat_picked = np.array(radiomics_feat_list)[np.array(current_idx_feat_picked)]
					idx_feat_picked_training_400.append(current_idx_feat_picked)
					feat_picked_training_400.append(current_feat_picked)
					X_tr = np.copy(X_new_tr[:,np.array(current_idx_feat_picked)])
					X_val = np.copy(X_new_val[:,np.array(current_idx_feat_picked)])
					lr_training_400_infold_with_fs = GridSearchCV(linear_model.LogisticRegression(penalty='l2',solver='lbfgs',max_iter = 20000), param_grid, cv=embedded_cv,scoring='roc_auc', n_jobs = num_cpus_to_load)
					_=lr_training_400_infold_with_fs.fit(X_tr,Y_tr)
					best_c_now = lr_training_400_infold_with_fs.best_params_['C']
					best_c_training_400_cv_with_fs.extend([best_c_now])
					val_proba_now_with_fs = lr_training_400_infold_with_fs.predict_proba(X_val)[:,1]
					wri_cv_with_fs.writerow(val_proba_now_with_fs.tolist())
					auc_training_400_cv_with_fs[iter2,nf] = metrics.roc_auc_score(Y_val,val_proba_now_with_fs)
				else:
					auc_training_400_cv_with_fs[iter2,nf] = 0.5
			print('--------- CV Only Logistic Regression Run %d NO fs AUC: %0.3f +- %0.3f; WITH fs AUC: %0.3f +- %0.3f' \
				%(iter2+1, round(auc_training_400_cv_nofs[:iter2+1].mean(),3),round(auc_training_400_cv_nofs[:iter2+1].mean(1).std(),3), \
					round(auc_training_400_cv_with_fs[:iter2+1].mean(),3),round(auc_training_400_cv_with_fs[:iter2+1].mean(1).std(),3)) )
			f_log.write('--------- CV Only Logistic Regression Run %d NO fs AUC: %0.3f +- %0.3f; WITH fs AUC: %0.3f +- %0.3f\n' \
				%(iter2+1, round(auc_training_400_cv_nofs[:iter2+1].mean(),3),round(auc_training_400_cv_nofs[:iter2+1].mean(1).std(),3), \
					round(auc_training_400_cv_with_fs[:iter2+1].mean(),3),round(auc_training_400_cv_with_fs[:iter2+1].mean(1).std(),3)) )
		np.save(saved_log_path+'CV_ONLY_Training_NO_FS_400_LogisticRegression_c.npy',best_c_training_400_cv)
		np.save(saved_log_path+'CV_ONLY_Training_WITH_FS_400_LogisticRegression_c.npy',best_c_training_400_cv_with_fs)
		np.save(saved_log_path+'CV_ONLY_Training_WITH_FS_400_LogisticRegression_picked_features_idx.npy',idx_feat_picked_training_400)

		f_feat_picked = open(saved_log_path + 'CV_ONLY_Training_WITH_FS_400_LogisticRegression_picked_features_names.txt','w')
		for one_select in feat_picked_training_400:
			f_feat_picked.write(' + '.join(one_select)+'\n')
		f_feat_picked.close()
		f_log.close()
		f_csv_cv_labels.close()
		f_csv_cv_nofs.close()
		f_csv_cv_with_fs.close()

		#########################CV Top Features Performance###############
		selected_features_num = 11
		f_log = open(saved_log_path + 'logs_CV_ONLY_selected_TOP_%dFeatures_saving_predictions.log'%(selected_features_num),'a')
		f_csv_cv_labels = open(saved_log_path + 'With_%dTOP_features_from_CV_feature_selection_labels.csv'%(selected_features_num),'w')
		wri_cv_labels = csv.writer(f_csv_cv_labels)
		best_c_training_400_cv_with_fs = []
		auc_training_400_top_feats = np.zeros([iterTimes,nfold])
		f_csv_cv_with_fs = open(saved_log_path + 'With_%dTOP_features_from_CV_feature_selection_predictions.csv'%(selected_features_num),'w')
		wri_cv_with_fs = csv.writer(f_csv_cv_with_fs)
		##
		idx_feat_picked_modelA_400 = np.load(saved_log_path+'CV_ONLY_Training_WITH_FS_400_LogisticRegression_picked_features_idx.npy')
		flatten_idx_feat_picked_modelA_400 = []
		for one_select in idx_feat_picked_modelA_400:
			flatten_idx_feat_picked_modelA_400.extend(one_select)

		uni_feat,counts_feat = np.unique(flatten_idx_feat_picked_modelA_400,return_counts = True)
		sorted_feat = uni_feat[np.flipud(np.argsort(counts_feat))]
		sorted_counts = np.flipud(np.sort(counts_feat))

		X_training_400_subset = np.copy(X_training_400_radiomics[:,sorted_feat[:selected_features_num]])
		current_feat_picked = np.array(radiomics_feat_list)[sorted_feat[:selected_features_num]].tolist()
		##
		num_training_pure = np.where(Y_training_400==0)[0].shape[0]
		num_training_upstaging = np.where(Y_training_400==1)[0].shape[0]
		num_holdout_pure = int(round(num_training_pure/float(nfold)))
		num_holdout_upstaging = int(round(num_training_upstaging/float(nfold)))
		ind0 = [indsub for indsub,x in enumerate(Y_training_400) if x==0]
		ind1 = [indsub for indsub,x in enumerate(Y_training_400) if x==1]
		for iter2,seed_val in zip(range(iterTimes),range(iterTimes)):	
			random.seed((iterTimes-seed_val)*2);random.shuffle(ind0)	
			random.seed((iterTimes-seed_val)*2);random.shuffle(ind1)
			for nf in range(nfold):
				ind_val = ind0[nf*num_holdout_pure:nf*num_holdout_pure+num_holdout_pure]+ind1[nf*num_holdout_upstaging:nf*num_holdout_upstaging+num_holdout_upstaging]
				ind_tr = list(set(ind0+ind1)-set(ind_val))
				X_new_tr = np.copy(X_training_400_subset[np.array(ind_tr),:])
				Y_tr = np.array(Y_training_400[np.array(ind_tr)])
				X_new_val = np.copy(X_training_400_subset[np.array(ind_val),:])
				Y_val = np.array(Y_training_400[np.array(ind_val)])
				wri_cv_labels.writerow(Y_val.tolist())
				X_tr = np.copy(X_new_tr);X_val = np.copy(X_new_val)
				lr_baseline_infold_with_fs = GridSearchCV(linear_model.LogisticRegression(penalty='l2',solver='lbfgs',max_iter = 10000), param_grid, cv=embedded_cv,scoring='roc_auc', n_jobs = num_cpus_to_load)
				_=lr_baseline_infold_with_fs.fit(X_tr,Y_tr)
				best_c_now = lr_baseline_infold_with_fs.best_params_['C']
				best_c_training_400_cv_with_fs.extend([best_c_now])
				val_proba_now_with_fs = lr_baseline_infold_with_fs.predict_proba(X_val)[:,1]
				wri_cv_with_fs.writerow(val_proba_now_with_fs.tolist())
				auc_training_400_top_feats[iter2,nf] = metrics.roc_auc_score(Y_val,val_proba_now_with_fs)
			print('--------- Feature Num: %d, Logistic Regression Run %d  auc: %0.3f +- %0.3f' %(selected_features_num,iter2+1, round(auc_training_400_top_feats[:iter2+1].mean(),3),round(auc_training_400_top_feats[:iter2+1].mean(1).std(),3)))
			f_log.write('--------- Feature Num: %d, Logistic Regression Run %d auc: %0.3f +- %0.3f\n' %(selected_features_num, iter2+1, round(auc_training_400_top_feats[:iter2+1].mean(),3),round(auc_training_400_top_feats[:iter2+1].mean(1).std(),3)))
		print('---- %d Number of TOP Features, Logistic Regression auc: %0.3f +- %0.3f, with Features: %s' %(selected_features_num, round(auc_training_400_top_feats.mean(),3),round(auc_training_400_top_feats.mean(1).std(),3),current_feat_picked))
		f_log.write('---- %d Number of TOP Features, Logistic Regression auc: %0.3f +- %0.3f, with Features: %s\n' %(selected_features_num, round(auc_training_400_top_feats.mean(),3),round(auc_training_400_top_feats.mean(1).std(),3),current_feat_picked))
		##
		np.save(saved_log_path + 'With_%dTOP_features_from_CV_feature_selection_best_parameters_C.npy'%(selected_features_num),best_c_training_400_cv_with_fs)
		f_csv_cv_labels.close()
		f_csv_cv_with_fs.close()

		#########################CV AND CLINICAL FEATURES Logistic Regression with Feature Selection and Top Features Performance###############
		f_log = open(saved_log_path + 'logs_CLINICAL_plus_CV_LR_WITH_and_WITHOUT_feature_selection.log','a')
		## Model A on 400 Training Data with Logistic Regression
		num_training_pure = np.where(Y_training_400==0)[0].shape[0]
		num_training_upstaging = np.where(Y_training_400==1)[0].shape[0]
		num_holdout_pure = int(round(num_training_pure/float(nfold)))
		num_holdout_upstaging = int(round(num_training_upstaging/float(nfold)))
		##
		f_csv_cv_and_cli_labels = open(saved_log_path + 'CV_AND_Clinical_Labels_with_%dfolds_%diterations.csv'%(nfold,iterTimes),'w')
		wri_cv_and_cli_labels = csv.writer(f_csv_cv_and_cli_labels)
		##
		best_c_training_400_cli_and_cv = []
		auc_training_400_cli_and_cv_nofs = np.zeros([iterTimes,nfold])
		f_csv_cv_and_cli_nofs = open(saved_log_path + 'CV_AND_Clinical_NO-FS_Predictions_with_%dfolds_%diterations.csv'%(nfold,iterTimes),'w')
		wri_cv_and_cli_nofs = csv.writer(f_csv_cv_and_cli_nofs)
		##
		best_c_training_400_cli_and_cv_with_fs = []
		auc_training_400_cli_and_cv_with_fs = np.zeros([iterTimes,nfold])
		f_csv_cv_and_cli_with_fs = open(saved_log_path + 'CV_AND_Clinical_WITH-FS_Predictions_with_%dfolds_%diterations.csv'%(nfold,iterTimes),'w')
		wri_cv_and_cli_with_fs = csv.writer(f_csv_cv_and_cli_with_fs)
		##
		idx_feat_picked_training_400 = []
		feat_picked_training_400 = []
		ind0 = [indsub for indsub,x in enumerate(Y_training_400) if x==0]
		ind1 = [indsub for indsub,x in enumerate(Y_training_400) if x==1]
		for iter2,seed_val in zip(range(iterTimes),range(iterTimes)):	
			random.seed((iterTimes-seed_val)*2);random.shuffle(ind0)	
			random.seed((iterTimes-seed_val)*2);random.shuffle(ind1)
			for nf in range(nfold):
				ind_val = ind0[nf*num_holdout_pure:nf*num_holdout_pure+num_holdout_pure]+ind1[nf*num_holdout_upstaging:nf*num_holdout_upstaging+num_holdout_upstaging]
				ind_tr = list(set(ind0+ind1)-set(ind_val))
				X_new_tr = np.copy(X_training_400_radiomics_and_clinical[np.array(ind_tr),:])
				Y_tr = np.array(Y_training_400[np.array(ind_tr)])
				X_new_val = np.copy(X_training_400_radiomics_and_clinical[np.array(ind_val),:])
				Y_val = np.array(Y_training_400[np.array(ind_val)])
				wri_cv_and_cli_labels.writerow(Y_val.tolist())
				X_tr = np.copy(X_new_tr);X_val = np.copy(X_new_val)
				lr_baseline_infold = GridSearchCV(linear_model.LogisticRegression(penalty='l2',solver='lbfgs',max_iter = 10000), param_grid, cv=embedded_cv,scoring='roc_auc', n_jobs = num_cpus_to_load)
				_=lr_baseline_infold.fit(X_tr,Y_tr)
				best_c_now = lr_baseline_infold.best_params_['C']
				best_c_training_400_cli_and_cv.extend([best_c_now])
				val_proba_now = lr_baseline_infold.predict_proba(X_val)[:,1]
				wri_cv_and_cli_nofs.writerow(val_proba_now.tolist())
				auc_training_400_cli_and_cv_nofs[iter2,nf] = metrics.roc_auc_score(Y_val,val_proba_now)
				##
				randomized_logistic = RandomizedLogisticRegression(C=1,random_state=nfold)
				randomized_logistic.fit(X_tr,Y_tr)
				ind_fea = np.array([i[0] for i in randomized_logistic.all_scores_])
				rank_one_ind_fea = np.flipud(np.argsort(ind_fea))
				current_idx_feat_picked = [i for i in rank_one_ind_fea if ind_fea[i]>0]
				if len(current_idx_feat_picked) > 0:
					current_feat_picked = np.array(radiomics_feat_list + clinical_feat_list)[np.array(current_idx_feat_picked)]
					idx_feat_picked_training_400.append(current_idx_feat_picked)
					feat_picked_training_400.append(current_feat_picked)
					X_tr = np.copy(X_new_tr[:,np.array(current_idx_feat_picked)])
					X_val = np.copy(X_new_val[:,np.array(current_idx_feat_picked)])
					lr_training_400_infold_with_fs = GridSearchCV(linear_model.LogisticRegression(penalty='l2',solver='lbfgs',max_iter = 10000), param_grid, cv=embedded_cv,scoring='roc_auc', n_jobs = num_cpus_to_load)
					_=lr_training_400_infold_with_fs.fit(X_tr,Y_tr)
					best_c_now = lr_training_400_infold_with_fs.best_params_['C']
					best_c_training_400_cli_and_cv_with_fs.extend([best_c_now])
					val_proba_now_with_fs = lr_training_400_infold_with_fs.predict_proba(X_val)[:,1]
					wri_cv_and_cli_with_fs.writerow(val_proba_now_with_fs.tolist())
					auc_training_400_cli_and_cv_with_fs[iter2,nf] = metrics.roc_auc_score(Y_val,val_proba_now_with_fs)
				else:
					auc_training_400_cli_and_cv_with_fs[iter2,nf] = 0.5
			print('--------- Clinical And CV Logistic Regression Run %d NO fs AUC: %0.3f +- %0.3f; WITH fs AUC: %0.3f +- %0.3f' \
				%(iter2+1, round(auc_training_400_cli_and_cv_nofs[:iter2+1].mean(),3), \
					round(auc_training_400_cli_and_cv_nofs[:iter2+1].mean(1).std(),3), \
					round(auc_training_400_cli_and_cv_with_fs[:iter2+1].mean(),3),round(auc_training_400_cli_and_cv_with_fs[:iter2+1].mean(1).std(),3)) )
			f_log.write('--------- Clinical And CV Logistic Regression Run %d NO fs AUC: %0.3f +- %0.3f; WITH fs AUC: %0.3f +- %0.3f\n' \
				%(iter2+1, round(auc_training_400_cli_and_cv_nofs[:iter2+1].mean(),3), \
					round(auc_training_400_cli_and_cv_nofs[:iter2+1].mean(1).std(),3), \
					round(auc_training_400_cli_and_cv_with_fs[:iter2+1].mean(),3),round(auc_training_400_cli_and_cv_with_fs[:iter2+1].mean(1).std(),3)) )
		np.save(saved_log_path+'CV_AND_Clinical_Training_NO_FS_400_LogisticRegression_c.npy',best_c_training_400_cli_and_cv)
		np.save(saved_log_path+'CV_AND_Clinical_Training_WITH_FS_400_LogisticRegression_c.npy',best_c_training_400_cli_and_cv_with_fs)
		np.save(saved_log_path+'CV_AND_Clinical_Training_WITH_FS_400_LogisticRegression_picked_features_idx.npy',idx_feat_picked_training_400)
		##
		f_feat_picked = open(saved_log_path + 'Clinical_not_categorical_and_CV_Training_WITH_FS_400_LogisticRegression_picked_features_names.txt','w')
		for one_select in feat_picked_training_400:
			f_feat_picked.write(' + '.join(one_select)+'\n')
		f_feat_picked.close()
		##
		f_log.close()
		f_csv_cv_and_cli_labels.close()
		f_csv_cv_and_cli_nofs.close()
		f_csv_cv_and_cli_with_fs.close()
		####################### FINAL Testing, On Training Set#######################
		# info_training_400
		############################ AUC Performance On TEST ############################
		## Model A + No FS on Clinical Features
		best_c_cli_only = np.load(saved_log_path + 'CLINICAL_FOUR_only_Training_NO-FS_best_parameters_C.npy')
		uni_c_cli_only,counts_c_cli_only = np.unique(best_c_cli_only,return_counts = True)
		most_selected_c_cli = uni_c_cli_only[np.flipud(np.argsort(counts_c_cli_only))][0]
		lr_baseline_cli_only = linear_model.LogisticRegression(penalty='l2',solver='lbfgs',max_iter = 10000,C = most_selected_c_cli)
		_=lr_baseline_cli_only.fit(X_training_400_clinical,Y_training_400)
		proba_test_300_cli = lr_baseline_cli_only.predict_proba(X_testing_300_clinical)[:,1]
		auc_test_modela_cli_only = metrics.roc_auc_score(Y_testing_300,proba_test_300_cli)
		## Model A + No FS on CV
		best_c_modela_400_cv_only = np.load(saved_log_path + 'CV_ONLY_Training_NO_FS_400_LogisticRegression_c.npy')
		uni_c_cv_only,counts_c_cv_only = np.unique(best_c_modela_400_cv_only,return_counts = True)
		most_selected_c_cv_only = uni_c_cv_only[np.flipud(np.argsort(counts_c_cv_only))][0]
		lr_modela_cv_only_nofs = linear_model.LogisticRegression(penalty='l2',solver='lbfgs',max_iter = 10000,C = most_selected_c_cv_only)
		_=lr_modela_cv_only_nofs.fit(X_training_400_radiomics,Y_training_400)
		proba_test_300_cv = lr_modela_cv_only_nofs.predict_proba(X_testing_300_radiomics)[:,1]
		auc_test_modela_cv_nofs = metrics.roc_auc_score(Y_testing_300,proba_test_300_cv)
		## Model A + Top Feats on CV 
		selected_features_cv_only_num = 11
		best_c_training_400_cv_top_features = np.load(saved_log_path + 'With_%dTOP_features_from_CV_feature_selection_best_parameters_C.npy'%(selected_features_cv_only_num))
		idx_feat_picked_modelA_400 = np.load(saved_log_path+'CV_ONLY_Training_WITH_FS_400_LogisticRegression_picked_features_idx.npy')
		flatten_idx_feat_picked_modelA_400 = []
		for one_select in idx_feat_picked_modelA_400:
		  flatten_idx_feat_picked_modelA_400.extend(one_select)

		uni_feat_cv,counts_feat_cv = np.unique(flatten_idx_feat_picked_modelA_400,return_counts = True)
		sorted_feat_cv = uni_feat_cv[np.flipud(np.argsort(counts_feat_cv))]
		sorted_counts_cv = np.flipud(np.sort(counts_feat_cv))
		##
		X_training_400_radiomics_subset = np.copy(X_training_400_radiomics[:,sorted_feat_cv[:selected_features_cv_only_num]])
		X_testing_300_radiomics_subset = np.copy(X_testing_300_radiomics[:,sorted_feat_cv[:selected_features_cv_only_num]])
		uni_c_cv_only_topfeats,counts_c_cv_only_topfeats = np.unique(best_c_training_400_cv_top_features,return_counts = True)
		most_selected_c_cv_only_top_feats = uni_c_cv_only_topfeats[np.flipud(np.argsort(counts_c_cv_only_topfeats))][0]
		lr_modela_cv_only_topfeats = linear_model.LogisticRegression(penalty='l2',solver='lbfgs',max_iter = 10000,C = most_selected_c_cv_only_top_feats)
		_=lr_modela_cv_only_topfeats.fit(X_training_400_radiomics_subset,Y_training_400)
		proba_test_300_top_feats = lr_modela_cv_only_topfeats.predict_proba(X_testing_300_radiomics_subset)[:,1]
		auc_test_modela_cv_only_top_feats = metrics.roc_auc_score(Y_testing_300,proba_test_300_top_feats) 
		## Model A + No FS on CV + Clinical
		best_c_modela_400_cli_and_cv = np.load(saved_log_path + 'CV_AND_Clinical_Training_NO_FS_400_LogisticRegression_c.npy')
		uni_c_cli_and_c,counts_c_cli_and_c = np.unique(best_c_modela_400_cli_and_cv,return_counts = True)
		most_selected_c_cv_and_cli = uni_c_cli_and_c[np.flipud(np.argsort(counts_c_cli_and_c))][0]
		lr_modela_cv_and_cli_nofs = linear_model.LogisticRegression(penalty='l2',solver='lbfgs',max_iter = 10000,C = most_selected_c_cv_and_cli)
		_=lr_modela_cv_and_cli_nofs.fit(X_training_400_radiomics_and_clinical,Y_training_400)
		proba_test_300_cv_and_cli = lr_modela_cv_and_cli_nofs.predict_proba(X_testing_300_radiomics_and_clinical)[:,1]
		auc_test_modela_cv_and_cli_nofs = metrics.roc_auc_score(Y_testing_300,proba_test_300_cv_and_cli) 
		################
		print('****************##########************* Final Results ****************##########*************')
		print('Random Seed\tTraining Clinical Features Only AUC\tTraining Radiomics Features Only AUC\tTraining Radiomics Features With Feature Selection AUC\tTraining Radiomics Top Features AUC\tTraining Radiomics + Clinical Features AUC\tTraining Radiomics + Clinical Features With Feature Selection AUC\tTesting Clinical Features Only AUC\tTesting Radiomics Features Only AUC\tTesting Radiomics Top Feats AUC\tTesting Radiomics + Clinical Features AUC')
		print('%d\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f' \
		%(random_seed_for_train_test_reshuffle, auc_training_400_cli_nofs.mean(), auc_training_400_cv_nofs.mean(), auc_training_400_cv_with_fs.mean(), \
			auc_training_400_top_feats.mean(), auc_training_400_cli_and_cv_nofs.mean(), auc_training_400_cli_and_cv_with_fs.mean(), \
			auc_test_modela_cli_only, auc_test_modela_cv_nofs, auc_test_modela_cv_only_top_feats, auc_test_modela_cv_and_cli_nofs))
		print('****************##########************* DONE ONE SHUFFLING ****************##########*************')
		print('****************##########************* DONE ONE SHUFFLING ****************##########*************')
		##
		with open(saved_log_path + 'Final_test300_from_train400_pred_and_labels_3models_seed%d_{Cli-CV-CV+CLI}_{%.3f, %.3f, %.3f}.csv'\
			%(random_seed_for_train_test_reshuffle, auc_test_modela_cli_only, auc_test_modela_cv_nofs, auc_test_modela_cv_and_cli_nofs),'w') as f_csv_final_test300:
			wri_csv_final_test300 = csv.writer(f_csv_final_test300)
			_= wri_csv_final_test300.writerow(['ID','labels','pred.cli.only','pred.CV','pred.top.feats','pred.CV.plus.clinical'])
			_=[wri_csv_final_test300.writerow([info_testing_300[idss][0],Y_testing_300[idss], proba_test_300_cli[idss], proba_test_300_cv[idss], proba_test_300_top_feats[idss], proba_test_300_cv_and_cli[idss]]) for idss in range(Y_testing_300.shape[0])]
		##
		with open(all_train_test_results_per_shuffle, 'a') as f_train_test_results_reshuffled:
			rea_train_test_results_reshuffled = csv.writer(f_train_test_results_reshuffled)
			rea_train_test_results_reshuffled.writerow([random_seed_for_train_test_reshuffle, p_age, p_lesion, auc_training_400_cli_nofs.mean(), auc_training_400_cv_nofs.mean(), auc_training_400_cv_with_fs.mean(), auc_training_400_top_feats.mean(), auc_training_400_cli_and_cv_nofs.mean(), auc_training_400_cli_and_cv_with_fs.mean(), auc_test_modela_cli_only, auc_test_modela_cv_nofs, auc_test_modela_cv_only_top_feats, auc_test_modela_cv_and_cli_nofs])


